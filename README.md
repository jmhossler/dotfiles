# jmhossler/dotfiles
Just a bunch of dotfiles.

## Install
Clone and symlink or install with [ellipsis][ellipsis]:

```
$ ellipsis install jmhossler/dotfiles
```

[ellipsis]: http://ellipsis.sh
